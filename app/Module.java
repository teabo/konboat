import com.google.inject.AbstractModule;

import services.CustomerService;
import services.OrderService;
import services.dummy.CustomerServiceDummy;
import services.dummy.OrderServiceDummy;

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 *
 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
public class Module extends AbstractModule {

    @Override
    public void configure() {
        // bind order service to dummy implementation
        bind(OrderService.class).to(OrderServiceDummy.class);
        // bind customer service to dummy implementation
        bind(CustomerService.class).to(CustomerServiceDummy.class);
    }

}
